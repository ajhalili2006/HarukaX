FROM golang

WORKDIR /go/src/gitlab.com/HarukaNetwork/OSS/HarukaX

COPY . .

ENTRYPOINT ["go", "run", "."]
